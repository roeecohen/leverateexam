package com.example.exam;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.exam.search_engine.DataManager;
import com.example.exam.search_engine.rest.RestResponseListener;
import com.example.exam.search_engine.view.KeyDescriptionFragment;
import com.example.exam.search_engine.view.SearchValuesAdapter;

public class MainActivity extends AppCompatActivity implements SearchValuesAdapter.OnCellClickedLister {

    RecyclerView searchValuesRecyclerView;
    KeyDescriptionFragment descriptionFragment;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();

        /**/
    }

    private void initRecyclerView () {
        searchValuesRecyclerView = (RecyclerView) findViewById(R.id.search_values_recycler);
        searchValuesRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        descriptionFragment = (KeyDescriptionFragment) getSupportFragmentManager().findFragmentById(R.id.description_fragment);
        SearchValuesAdapter adapter = new SearchValuesAdapter();
        adapter.setCellClickedLister(this);
        searchValuesRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onCellClicked (int adapterPosition, String queryValue) {
        //if (descriptionFragment != null) descriptionFragment.setAndUpdateDescription(queryValue);
        if (descriptionFragment != null) descriptionFragment.searchValue(queryValue);
    }
}
