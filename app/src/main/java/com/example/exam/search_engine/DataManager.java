package com.example.exam.search_engine;

import android.support.annotation.NonNull;

import com.example.exam.search_engine.rest.RestApiManager;
import com.example.exam.search_engine.rest.RestResponseListener;
import com.example.exam.search_engine.rest.retrofitRest.RetrofitManager;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Roee on 03/07/2017.
 */

public class DataManager {

    RestApiManager restApiManager;
    private static DataManager dataManagerInstance;

    public static DataManager getInstance () {
        if (dataManagerInstance == null) {
            dataManagerInstance = new DataManager();
        }
        return dataManagerInstance;
    }

    private DataManager () {
        restApiManager = RetrofitManager.getInstance();
    }

    public void jsonQuery(String queryValue, final RestResponseListener.DescriptionListener descriptionListener){
        if (descriptionListener == null || queryValue == null || queryValue.isEmpty()) {
            return;
        }

        restApiManager.query(queryValue, new RestResponseListener.DescriptionListener() {
            @Override
            public void OnQueryDescriptionResult (@NonNull String description) {
                descriptionListener.OnQueryDescriptionResult(description);
            }
        });
    }

    public ArrayList<String> getSearchKeysInitialValues () {
         return new ArrayList<String>(Arrays.asList("Mac", "Dog", "Bag", "Car", "Cow", "Day"));
    }

}
