package com.example.exam.search_engine.rest;

/**
 * Created by Roee on 03/07/2017.
 */

public class Config {
    public static final String BASE_URL = "http://api.duckduckgo.com/";
    public static final String FORMAT_AS_JSON = "?format=json";
}
