package com.example.exam.search_engine.rest;

/**
 * Created by Roee on 03/07/2017.
 */

public interface RestApiManager {

    void query(String queryValue, RestResponseListener.DescriptionListener descriptionListener);

}
