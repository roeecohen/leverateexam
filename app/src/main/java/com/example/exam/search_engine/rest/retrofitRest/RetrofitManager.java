package com.example.exam.search_engine.rest.retrofitRest;

import com.example.exam.search_engine.model.JsonQueryResponse;
import com.example.exam.search_engine.model.RelatedTopic;
import com.example.exam.search_engine.rest.Config;
import com.example.exam.search_engine.rest.RestApiManager;
import com.example.exam.search_engine.rest.RestResponseListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Roee on 03/07/2017.
 */

public class RetrofitManager implements RestApiManager {

    private RetrofitRestCalls service;
    private static RetrofitManager retrofitManagerInstance;

    public static RetrofitManager getInstance () {
        if (retrofitManagerInstance == null) {
            retrofitManagerInstance = new RetrofitManager();
        }
        return retrofitManagerInstance;
    }

    private RetrofitManager () {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(RetrofitRestCalls.class);
    }


    @Override
    public void query (final String queryValue, final RestResponseListener.DescriptionListener descriptionListener) {

        Call<JsonQueryResponse> jsonObjectCall = service.queryWithJsonResponse(queryValue);

        jsonObjectCall.enqueue(new Callback<JsonQueryResponse>() {
            @Override
            public void onResponse (Call<JsonQueryResponse> call, Response<JsonQueryResponse> response) {
                if (descriptionListener == null || response.body() == null || response.body().getRelatedTopics() == null) {
                    return;
                } else {

                    ArrayList<RelatedTopic> relatedTopics = (ArrayList<RelatedTopic>) response.body().getRelatedTopics();

                    if (relatedTopics.size() == 0) {
                        descriptionListener.OnQueryDescriptionResult("No Results Found");
                    } else {
                        RelatedTopic firstTopic = relatedTopics.get(0);
                        if (firstTopic != null && firstTopic.getText() != null) {
                            descriptionListener.OnQueryDescriptionResult(firstTopic.getText());
                        } else {
                            descriptionListener.OnQueryDescriptionResult("there is no text for the value " + queryValue);
                        }
                    }

                }
            }

            @Override
            public void onFailure (Call<JsonQueryResponse> call, Throwable t) {
                if (descriptionListener != null) descriptionListener.OnQueryDescriptionResult("Failed in get request");
            }
        });
    }
}
