package com.example.exam.search_engine.rest.retrofitRest;

import com.example.exam.search_engine.model.JsonQueryResponse;
import com.example.exam.search_engine.rest.Config;

import retrofit2.Call;
import retrofit2.http.*;
/**
 * Created by Roee on 03/07/2017.
 */

public interface RetrofitRestCalls {

    @GET(Config.FORMAT_AS_JSON)
    Call<JsonQueryResponse> queryWithJsonResponse (@Query("q") String queryValue);
}
