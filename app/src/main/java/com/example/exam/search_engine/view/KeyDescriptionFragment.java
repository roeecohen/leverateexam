package com.example.exam.search_engine.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.exam.R;
import com.example.exam.search_engine.DataManager;
import com.example.exam.search_engine.rest.RestResponseListener;

/**
 * Created by Roee on 03/07/2017.
 */

public class KeyDescriptionFragment extends Fragment {

    private String description;
    private TextView descriptionTextView;
    ProgressBar descriptionProgress;

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FrameLayout descriptionLayout = (FrameLayout) inflater.inflate(R.layout.key_descreption_layout, container, false);
        descriptionTextView = (TextView) descriptionLayout.findViewById(R.id.description_text_view);
        descriptionProgress = (ProgressBar) descriptionLayout.findViewById(R.id.description_progress);
        return descriptionLayout;
    }

    @Override
    public void onViewCreated (View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateDescription();
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public void updateDescription () {
        if (description != null && descriptionTextView != null) {
            descriptionTextView.setText(description);
        }
    }

    public void setAndUpdateDescription (String description) {
        setDescription(description);
        updateDescription();
    }

    public void searchValue (String queryValue) {

        showProgress();

        DataManager.getInstance().jsonQuery(queryValue, new RestResponseListener.DescriptionListener() {
            @Override
            public void OnQueryDescriptionResult (String description) {
                showText();
                setAndUpdateDescription(description);
            }
        });
    }

    private void showProgress() {
        toggleViewsVisibility(View.GONE, View.VISIBLE);
    }

    private void showText() {
        toggleViewsVisibility(View.VISIBLE, View.GONE);
    }

    private void toggleViewsVisibility(int textVisibility, int progressVisibility) {
        if (descriptionTextView != null && descriptionProgress != null) {
            descriptionTextView.setVisibility(textVisibility);
            descriptionProgress.setVisibility(progressVisibility);
        }
    }
}
