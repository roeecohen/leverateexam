package com.example.exam.search_engine.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.exam.R;
import com.example.exam.search_engine.DataManager;

import java.util.ArrayList;

/**
 * Created by Roee on 03/07/2017.
 */

public class SearchValuesAdapter extends RecyclerView.Adapter<SearchValuesAdapter.ViewHolder> {

    private ArrayList<String> keys;
    private OnCellClickedLister cellClickedLister;

    public SearchValuesAdapter () {
        this.keys = DataManager.getInstance().getSearchKeysInitialValues();
    }

    @Override
    public ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        LinearLayout textLayout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.key_layout, parent, false);
        TextView tv = (TextView) textLayout.findViewById(R.id.key_text_view);
        return new ViewHolder(textLayout, tv);
    }

    @Override
    public void onBindViewHolder (ViewHolder holder, int position) {
        if (keys == null) {
            holder.keyTextView.setText("null");
        } else {
            String s = keys.get(position);
            holder.keyTextView.setText(s);
        }
    }

    @Override
    public int getItemCount () {
        return keys != null ? keys.size() : 0;
    }

    public void setCellClickedLister (OnCellClickedLister cellClickedLister) {
        this.cellClickedLister = cellClickedLister;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView keyTextView;

        public ViewHolder (LinearLayout textLayout, TextView textView) {
            super(textLayout);
            keyTextView = textView;
            textLayout.setOnClickListener(this);
        }


        @Override
        public void onClick (View v) {

            if (cellClickedLister == null) {
                return;
            }

            final int position = getAdapterPosition();
            String queryValue = keys.get(position);
            cellClickedLister.onCellClicked(position, queryValue);
        }
    }

    public interface OnCellClickedLister {
        void onCellClicked(int adapterPosition, String queryValue);
    }

}
